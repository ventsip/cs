# Split text to multiple smaller chunks of certian maximum length

Given a `text`, split it to a smallest possible number of chunks, each of a maximum length of `n`, and add `"|x/y"` at the end of each chunk (where `x` is the chunk number and `y` is the total number of chunks).

- Do not cut the words in the middle. Split only at spaces (but *keep the spaces* in the text)
- Chunks should not exceed their maximum size `n` *with the added suffix*!
- Do not add the suffix, if `text` fits into a single chunk

## Examples

1. `"hello, world!"` with maximum chunk size of `n=12` should be split to two chunks: `"hello, |1/2"` and `"world!|2/2"`
2. `"hello, world!"` with maximum chunk size of `n=13` should produce a single chunk, containing the original text and wihtout any suffix added (since the text contains exactly `13` characters)
