package main

import (
	"fmt"
	"strconv"
	"testing"
)

// TestCountDigits - unit test for countDigits function
func TestCountDigits(t *testing.T) {
	tests := [][]int{
		{0, 1},
		{1, 1},
		{10, 2},
		{99, 2},
		{101, 3},
		{100001, 6},
		{-1, 1},
		{-9, 1},
		{-10, 2},
	}
	for _, test := range tests {
		if r := countDigits(test[0]); r != test[1] {
			t.Errorf("for %d expected %d, returned %d", test[0], test[1], r)
		}
	}
}

func testEqAndMaxLen(s1, s2 []string, l int) (equal bool) {
	if (s1 == nil) != (s2 == nil) {
		return false
	}

	if len(s1) != len(s2) {
		return false
	}

	for i := range s1 {
		if s1[i] != s2[i] {
			return false
		}
		if len(s1[i]) > l || len(s2[i]) > l {
			return false
		}
	}

	return true
}

type testT struct {
	l  int
	s  string
	r  []string
	ok bool
}

var tests = []testT{
	{12, "hello, world!",
		[]string{"hello, |1/2", "world!|2/2"}, true},
	{11, "hello hello world",
		[]string{"hello |1/3", "hello |2/3", "world|3/3"}, true},
	{12, "hello hello hello hello hello hello hello hello hello world",
		[]string{"hello |1/10", "hello |2/10", "hello |3/10", "hello |4/10", "hello |5/10", "hello |6/10", "hello |7/10", "hello |8/10", "hello |9/10", "world|10/10"}, true},
	{10, "12345 12345 12345 12345 12345 12345 12345 12345 12345 12345",
		[]string{}, false},
	{34, "12345 12345 12345 12345 12345 12345 12345 12345 12345 12345",
		[]string{"12345 12345 12345 12345 12345 |1/2", "12345 12345 12345 12345 12345|2/2"}, true},
	{58, "12345 12345 12345 12345 12345 12345 12345 12345 12345 12345",
		[]string{"12345 12345 12345 12345 12345 12345 12345 12345 12345 |1/2", "12345|2/2"}, true},
	{12, "123456789012",
		[]string{"123456789012"}, true},
	{11, "123456789012",
		[]string{}, false},
	{1, "123456789012",
		[]string{""}, false},
	{10, "he l l o, w o r l d !",
		[]string{"he l l|1/4", " o, w |2/4", "o r l |3/4", "d !|4/4"}, true},
}

func TestSplitText(t *testing.T) {

	for _, test := range tests {
		res, e := splitSms(test.s, test.l)

		if e != nil {
			if test.ok == true {
				t.Errorf("splitting string \"%s\" with max chunk size %d returned error: %s", test.s, test.l, e)
			}
		} else {
			if test.ok == true {
				if testEqAndMaxLen(res, test.r, test.l) == false {
					s := fmt.Sprintln("string \""+test.s+"\" with max chunk size", strconv.Itoa(test.l), "split incorrectly or returned bigger chunk")
					s = s + "Expected:\n"
					s = s + sprintStrings(test.r)
					s = s + "\nReceived:\n"
					s = s + sprintStrings(res)
					t.Errorf(s)
				}
			} else {
				s := fmt.Sprintln("splitting string \""+test.s+"\" with max chunk size", strconv.Itoa(test.l), "should have returned error, but didn't")
				s = s + "Received:\n"
				s = s + sprintStrings(res)
				t.Errorf(s)
			}
		}
	}
}

func BenchmarkSplitText(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, test := range tests {
			_, _ = splitSms(test.s, test.l)
		}
	}
}
