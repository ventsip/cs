# Benchmarks

## 12.10.2019

goos: darwin\
goarch: amd64\
pkg: bitbucket.org/ventsip/cs/split-sms\
BenchmarkSplitText-8       10000     142574 ns/op    32827 B/op     2306 allocs/op\
PASS
coverage: 81.6% of statements\
ok bitbucket.org/ventsip/cs/split-sms 1.648s\
Success: Benchmarks passed.
