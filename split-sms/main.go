package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// countDigits counts number of digits in an integer, excluding the sign symbol (if any)
func countDigits(i int) (count int) {

	count = 1
	i = i / 10

	for i != 0 {
		count++
		i = i / 10
	}

	return
}

// tryCutChunk tries to cut a chunk with number 'index' (out of 'maxChunks' chunks) from the beginning of the 'text'
func tryCutChunk(text string, maxChunkLen int, index int, maxChunks int) (chunk string, newHead string, e error) {
	if maxChunkLen < 1 || index < 0 || maxChunks < 1 {
		panic(errors.New("Invalid arguments"))
	}

	sLen := 1 + 1 + countDigits(index+1) + countDigits(maxChunks) // "|x/y"

	// check if the text with the suffix fits in the chunk
	if len(text)+sLen <= maxChunkLen {
		chunk = text + fmt.Sprintf("|%d/%d", index, maxChunks)
		return
	}

	for i := maxChunkLen - sLen; i >= 0; i-- {
		if text[i] == ' ' {
			next := i
			if next < maxChunkLen-sLen {
				next++
			}
			chunk = text[:next] + fmt.Sprintf("|%d/%d", index, maxChunks)
			newHead = text[next:]
			return
		}
	}

	return "", "", errors.New("Word doesn't fit into a single chunk")
}

// trySplitToChunks tries to split 'text' into 'maxChunks' chunks, each with max length of 'maxChunkLen'
func trySplitToChunks(text string, maxChunkLen int, maxChunks int) (result []string, e error) {

	if maxChunkLen < 1 || maxChunks < 1 {
		panic(errors.New("Invalid arguments"))
	}

	// the whole text fits in one chunk
	if len(text) <= maxChunkLen {
		result = append(result, text)
		return
	}

	head := text
	var chunk string
	for n := 1; n <= maxChunks; n++ {
		chunk, head, e = tryCutChunk(head, maxChunkLen, n, maxChunks)
		if e != nil {
			return
		}

		result = append(result, chunk)

		if len(head) == 0 {
			return
		}
	}

	return nil, errors.New("Number of chunks exceeded the limit")
}

// splitSms splits 'text' into chunks with maximum length of 'maxChunkLen', adding "|x/y" suffix
func splitSms(text string, maxChunkLen int) (result []string, e error) {
	if maxChunkLen < 1 {
		panic(errors.New("Invalid arguments"))
	}

	// start with smallest possible number of chunks and increase number of chunks until solution is found
	for chunks := (len(text) + maxChunkLen - 1) / maxChunkLen; chunks <= len(text); chunks++ {

		result, e = trySplitToChunks(text, maxChunkLen, chunks)

		if e == nil {
			return
		}
	}

	return
}

func sprintStrings(a []string) (s string) {
	for _, r := range a {
		s = s + "\"" + r + "\" [" + strconv.Itoa(len(r)) + "]\n"
	}
	return
}

func main() {
	// r, e := splitSms("hello hello hello hello hello hello hello hello hello world", 12)
	// a, e := splitSms("he l l o, w o r l d !", 10)

	m := ""
	var i int
	for i = 0; i < 45; i++ {
		m = m + strings.Repeat("*", i+1) + " "
	}

	a, e := splitSms(m, i*3)

	fmt.Println(e)
	fmt.Println(sprintStrings(a))
}
