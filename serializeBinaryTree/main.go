package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
)

type nodeT struct {
	Value int
	Left  *nodeT
	Right *nodeT
}

var counter int

func createTree(maxLevels uint, leftProb uint, rightProb uint) (node *nodeT) {
	if 0 == maxLevels {
		return
	}

	node = new(nodeT)

	node.Value = counter
	counter = counter + 1

	if rand.Intn(101) < int(leftProb) {
		node.Left = createTree(maxLevels-1, leftProb, rightProb)
	}
	if rand.Intn(101) < int(rightProb) {
		node.Right = createTree(maxLevels-1, leftProb, rightProb)
	}

	return
}

func printTree(node nodeT, ind int) {
	// Value
	fmt.Println(node.Value)
	// Left chile
	fmt.Print(strings.Repeat(". ", ind/2))
	fmt.Print(". ")
	if nil != node.Left {
		printTree(*node.Left, ind+2)
	} else {
		fmt.Println("nil")
	}
	// Right child
	fmt.Print(strings.Repeat(". ", ind/2))
	fmt.Print(". ")
	if nil != node.Right {
		printTree(*node.Right, ind+2)
	} else {
		fmt.Println("nil")
	}
}
func printTree1(node nodeT) {
	fmt.Print("(")
	// Value
	fmt.Print(node.Value, ",")
	// Left chile
	if nil != node.Left {
		printTree1(*node.Left)
	} else {
		fmt.Print("()")
	}
	fmt.Print(",")
	// Right child
	if nil != node.Right {
		printTree1(*node.Right)
	} else {
		fmt.Print("()")
	}
	fmt.Print(")")
}

func printTree2(node nodeT) {
	// Value
	fmt.Print(node.Value)
	fmt.Print(",")
	// Left child
	if nil != node.Left {
		printTree2(*node.Left)
	}
	fmt.Print(",")
	// Right child
	if nil != node.Right {
		printTree2(*node.Right)
	}
}

func printTree3(node nodeT) {
	jsonData, err := json.Marshal(node)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(string(jsonData))
	}
}

func main() {
	counter = 0
	t := createTree(4, 80, 80)

	printTree(*t, 0)

	printTree1(*t)
	fmt.Println()

	printTree2(*t)
	fmt.Println()

	printTree3(*t)
	fmt.Println()
}
