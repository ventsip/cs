# Check whether two numeric intervals overlap

Given two intervals `a` and `b`, defined by their end points `b` and `e` (which are integer numbers), check whether they overlap.
The intervals are closed, i.e. they include their end points `b` and `e`.

## Examples

### The following intervals overlap

- [1, 5] and [5, 8]
- [1, 6] and [5, 8]
- [1, 8] and [5, 8]
- [1, 9] and [5, 8]
- [5, 9] and [5, 8]
- [5, 7] and [5, 8]
- [6, 7] and [5, 8]
- [5, 8] and [5, 8]
- [6, 8] and [5, 8]
- [6, 9] and [5, 8]
- [8, 10] and [5, 8]
- [1, 5] and [5, 8]

### The following intervals do *not* overlap

- [1, 3] and [5, 8]
- [9, 10] and [5, 8]
