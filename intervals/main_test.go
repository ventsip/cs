package main

import "testing"

type testCase struct {
	i1, i2  interval
	overlap bool
}

var testcases = []testCase{
	{interval{1, 3}, interval{5, 8}, false},
	{interval{1, 5}, interval{5, 8}, true},
	{interval{1, 6}, interval{5, 8}, true},
	{interval{1, 8}, interval{5, 8}, true},
	{interval{1, 9}, interval{5, 8}, true},
	{interval{5, 9}, interval{5, 8}, true},
	{interval{5, 7}, interval{5, 8}, true},
	{interval{6, 7}, interval{5, 8}, true},
	{interval{5, 8}, interval{5, 8}, true},
	{interval{6, 8}, interval{5, 8}, true},
	{interval{6, 9}, interval{5, 8}, true},
	{interval{8, 10}, interval{5, 8}, true},
	{interval{9, 10}, interval{5, 8}, false},
}

var funcs = [](func(a, b interval) bool){
	CheckOverlap1,
	CheckOverlap2,
	CheckOverlap3,
	CheckOverlap4,
}

func testCheckOverlapFunc(f func(a, b interval) bool, t *testing.T) {
	for _, test := range testcases {
		b := f(test.i1, test.i2)
		if b != test.overlap {
			t.Error(&f, test.i1, test.i2, "expected", test.overlap, "returned", b)
		}
		// swap first and second intervals
		b = f(test.i2, test.i1)
		if b != test.overlap {
			t.Error(&f, test.i2, test.i1, "expected", test.overlap, "returned", b)
		}
	}
}

func TestCheckOverlap(t *testing.T) {
	for _, f := range funcs {
		testCheckOverlapFunc(f, t)
	}
}

var ret bool

func benchmarkOverlapFunc(f func(a, b interval) bool, b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, test := range testcases {
			for j := 0; j < 100; j++ {
				ret = f(test.i1, test.i2)
			}
		}
	}
}

func BenchmarkF1(b *testing.B) {
	benchmarkOverlapFunc(CheckOverlap1, b)
}

func BenchmarkF2(b *testing.B) {
	benchmarkOverlapFunc(CheckOverlap2, b)
}

func BenchmarkF3(b *testing.B) {
	benchmarkOverlapFunc(CheckOverlap3, b)
}

func BenchmarkF4(b *testing.B) {
	benchmarkOverlapFunc(CheckOverlap4, b)
}
