package main

type interval struct {
	b, e int // both ends are inclusive (we live in a quantum world)
}

// CheckOverlap1 - returns true if two intervals overlap
func CheckOverlap1(i1, i2 interval) bool {
	return !(i1.b > i2.e || i1.e < i2.b)
}

// CheckOverlap2 - same as CheckOverlap1 with applied De-Morgan rule
func CheckOverlap2(i1, i2 interval) bool {
	return i1.b <= i2.e && i1.e >= i2.b
}

// CheckOverlap3 - arithmetic check
func CheckOverlap3(i1, i2 interval) bool {
	return (i1.b-i2.b)*(i1.b-i2.e) <= 0 || (i2.b-i1.b)*(i2.b-i1.e) <= 0
}

// CheckOverlap4 - decision tree
func CheckOverlap4(i1, i2 interval) bool {
	if i1.b < i2.b {
		if i1.e < i2.b {
			return false
		} // i1.end >= i2.start
		return true
	}
	// i1.start >= i2.start
	if i1.b > i2.e {
		return false
	} // i1.start <= i2.end
	return true
}

func main() {
}
